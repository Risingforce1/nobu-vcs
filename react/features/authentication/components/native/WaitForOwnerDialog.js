// @flow

import React, { Component } from "react";
import { Image, Text, View } from "react-native";
import { Button, Paragraph, Dialog } from "react-native-paper";
import type { Dispatch } from "redux";

import { translate } from "../../../base/i18n";
import { connect } from "../../../base/redux";
import { openLoginDialog, cancelWaitForOwner } from "../../actions.native";
import { reloadNow } from "../../../../features/app/actions";

import ImgWaitForOwnerDialog from "../../../../../images/custom/WaitForOwnerDialog.jpeg";

/**
 * The type of the React {@code Component} props of {@link WaitForOwnerDialog}.
 */
type Props = {
    /**
     * The name of the conference room (without the domain part).
     */
    _room: string,

    /**
     * Redux store dispatch function.
     */
    dispatch: Dispatch<any>,

    /**
     * Invoked to obtain translated strings.
     */
    t: Function,
};

/**
 * The dialog is display in XMPP password + guest access configuration, after
 * user connects from anonymous domain and the conference does not exist yet.
 *
 * See {@link LoginDialog} description for more details.
 */
class WaitForOwnerDialog extends Component<Props> {
    /**
     * Initializes a new WaitForWonderDialog instance.
     *
     * @param {Object} props - The read-only properties with which the new
     * instance is to be initialized.
     */
    constructor(props) {
        super(props);

        // Bind event handlers so they are only bound once per instance.
        this._onCancel = this._onCancel.bind(this);
        this._onLogin = this._onLogin.bind(this);
        this._onRejoin = this._onRejoin.bind(this);
    }

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */

    componentDidMount() {
        var self = this;
        this.timerID = setInterval(() => {
            self._onRejoin();
        }, 100000);
    }
    
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    render() {
       // if (this.state.counter === 0) {
         //   this._onRejoin();
            // this.props.dispatch(reloadNow());
        //}

        const { _room: room } = this.props;

        return (
            // <ConfirmDialog
            //     cancelKey = 'dialog.Cancel' //untuk mengganti text authentication ok
            //     contentKey = {
            //         {
            //             key: 'dialog.WaitForHostMsgWOk',
            //             params: { room }
            //         }
            //     }
            //     okKey = 'dialog.Ok' //untuk mengganti text authentication cancel
            //     onCancel = { this._onCancel }
            //     onSubmit = { this._onLogin }
            //     />

            // <Dialog visible={true} dismissable={false}>
            //     {/* <Dialog.Title>Alert</Dialog.Title> */}
            //     <Dialog.Content style={{ alignItems: "center" }}>
            //         <Image
            //             source={ImgWaitForOwnerDialog}
            //             style={{ height: 200, width: 200 }}
            //         />
            //         {/* <Paragraph>
            //             Mohon menunggu untuk terhubung dengan Petugas Bank.
            //         </Paragraph> */}
            //     </Dialog.Content>
            //     <Dialog.Actions style={{ flexDirection: "column" }}>
            //         <Button onPress={this._onCancel}>Batalkan</Button>
            //         {/* <Button onPress={ this._onLogin }>Login</Button> */}
            //         {/* <Button
            //             onPress={this._onRejoin}
            //             loading={this.state.counter === 0}
            //         >
            //             {this.state.counter === 0
            //                 ? ""
            //                 : `Menghubungkan ... (${this.state.counter})`}
            //         </Button> */}
            //         {/*<Button onPress={ this._onRejoin }>Join</Button>*/}
            //     </Dialog.Actions>
            // </Dialog>
            null
        );
    }

    _onRejoin: () => void;

    /**
     * Called when the cancel button is clicked.
     *
     * @private
     * @returns {void}
     */
    _onRejoin() {
        this.props.dispatch(reloadNow());
    }

    _onCancel: () => void;

    /**
     * Called when the cancel button is clicked.
     *
     * @private
     * @returns {void}
     */
    _onCancel() {
        this.props.dispatch(cancelWaitForOwner());
    }

    _onLogin: () => void;

    /**
     * Called when the OK button is clicked.
     *
     * @private
     * @returns {void}
     */
    _onLogin() {
        this.props.dispatch(openLoginDialog());
    }
}

/**
 * Maps (parts of) the Redux state to the associated props for the
 * {@code WaitForOwnerDialog} component.
 *
 * @param {Object} state - The Redux state.
 * @private
 * @returns {Props}
 */
function _mapStateToProps(state) {
    const { authRequired } = state["features/base/conference"];

    return {
        _room: authRequired && authRequired.getName(),
    };
}

export default translate(connect(_mapStateToProps)(WaitForOwnerDialog));
